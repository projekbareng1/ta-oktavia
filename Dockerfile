FROM php:apache

# Install MySQLi extension
RUN docker-php-ext-install mysqli

# Enable mod_rewrite for Apache
RUN a2enmod rewrite

# Copy your PHP files to the container
COPY ./src /var/www/html

# Set the working directory
WORKDIR /var/www/html

# Adjust file permissions
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 755 /var/www/html