-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2023 at 01:44 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wisata`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_wisata`
--

CREATE TABLE `menu_wisata` (
  `id_wisata` int(11) NOT NULL,
  `nama_wisata` varchar(1000) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `informasi` text NOT NULL,
  `fasilitas` text NOT NULL,
  `alamat` text NOT NULL,
  `foto_wisata` varchar(1000) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu_wisata`
--

INSERT INTO `menu_wisata` (`id_wisata`, `nama_wisata`, `kategori`, `informasi`, `fasilitas`, `alamat`, `foto_wisata`, `lat`, `lng`) VALUES
(1, 'Ekowisata Mangrove', 'alam', 'Hutan Wisata Mangrove Surabaya merupakan wisata yang menggabungkan wisata rekreasi dan edukasi. Di area dengan luas kurang lebih 200 hektar ini ditanami berbagai tanaman bakau. Lokasi wisata ini dikelola dengan baik oleh pemerintah Kota Surabaya.  Wonorejo Ekowisata Mangrove ini memiliki daya tarik dari keasrian alamnya. Pada awalnya, kawasan konservasi alam ini dibuat untuk mencegah abrasi di wilayah Timur Kota Surabaya. Pengelolaan yang baik membuat pemerintah Kota Surabaya membuka lokasi ini untuk umum', 'Destination: Alam\r\nOpen: Setiap Hari : 08.00 – 16.00\r\nTiket: Gratis Sewa Kapal Dewasa : 25.000 Anak : 15.000\r\nFasilitas :  kolam pancing, pendopo, kantin, hingga sewa perahu \r\n', 'Jl. Raya Wonorejo No.1, Wonorejo, Kec. Rungkut, Kota Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_42_01.jpg', -7.30865, 112.813),
(2, 'Pantai Kenjeran', 'alam', 'Pantai Ria kenjeran biasa disebut pantai kenjeran lama merupakan salah satu tempat wisata di surabaya yang murah meriah, kalo dulu setiap kali lebaran menjadi primadona di warga sekitar. Kadang pantai ini sering tertukar dengan pantai kenjeran park (kenpark) atau sering disebut kenjeran baru. Sekarang sudah lebih mendingan fasilitasnya, banyak fasilitas wahana bagi anak-anak. Pepohonan juga sudah lebih rindang jadi sejuk', 'Destination: Alam\r\nOpen: Setiap Hari : 09.00 – 17.00\r\nTiket: Rp 15.000/orang\r\nFasilitas : Tempat parkir kendaraan luas, Pusat informasi, Warung wisata, Pusat kulineran, Toilet umum, Tempat ibadah, Spot foto instagenic, Gazebo, Tempat duduk, Wahana permainan menarik ', 'Jl. Pantai Ria Kenjeran, Kenjeran, Bulak, Kenjeran, Kec. Bulak, Kota Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_44_01.jpg', -7.23806, 112.787),
(3, 'Hutan Bambu Keputih', 'alam', 'Kebun Bambu Surabaya juga dikenal sebagai Kebun Bambu Keputih, karena lokasinya berada di Keputih. Kebun Bambu Keputih mengingatkan pada Sagano Bamboo Forest yang ada di Jepang. Berdiri di area 40 hektar, yang dibagi menjadi Hutan Bambu, Taman Harmoni, dan Taman Ruang Publik. Kebun Bambu Keputih menjadi pilihan tepat untuk menenangkan pikiran di tengah Kota Surabaya yang padat', 'Destination: Alam\r\nOpen: Setiap Hari : 24 jam\r\nTiket: Gratis \r\nFasilitas :    arena bermain dan juga gazebo\r\n', 'Jl. Raya Marina Asri, Keputih, Kec. Sukolilo, Kota Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_45_01.jpg', -7.29419, 112.802),
(4, 'Masjid Sunan Ampel', 'religi', 'Masjid Ampel didirikan tahun 1421 oleh Sunan Ampel, dibantu sahabat karibnya Mbah Sholeh dan Mbah Sonhaji, serta santrinya. Masjid ini dibangun di atas sebidang tanah seluas 120 x 180 meter persegi di Desa Ampel (sekarang Kelurahan Ampel), Kecamatan Semampir Surabaya atau sekitar 2 km ke arah Timur Jembatan Merah. Tidak disebut kapan selesainya pembangunan Masjid Ampel ini. Sunan Ampel juga mendirikan Pondok Pesantren Ampel. Sejak tahun 1972 Kawasan Masjid Agung Sunan Ampel telah ditetapkan menjadi tempat wisata religi oleh Pemkot Surabaya', 'Destinasi: Religi', 'Jl. Ampel Masjid No. 53\r\n', 'https://tourism.surabaya.go.id/storage/tour/1652858583_1.jpg', -7.23034, 112.734),
(5, 'Masjid Muhammad Cheng Hoo', 'religi', 'Masjid Cheng Ho Surabaya merupakan masjid pertama di Indonesia yang menggunakan nama Muslim Tionghoa dan menjadi simbol perdamaian umat beragama. Nama masjid ini berasal dari sosok penghormatan pada Cheng Ho, seorang Muslim berperawakan tinggi yang pernah berlayar dari China hingga ke pantai Afrika. Kedatangan Cheng Ho saat itu disambut baik karena ia menghormati wilayah yang ia singgahi.\r\nPada pintu masjid menyerupai pagoda. Di puncak pagoda pun terdapat relief naga serta patung singa yang terbuat dari lilin dengan lafaz Allah yang ditulis dengan menggunakan huruf Arab.\r\n', 'Destinasi: Religi', 'Jl. Gading No.2, Ketabang, Genteng\r\n', 'https://tourism.surabaya.go.id/storage/tour/1652849321_1.jpg', -7.25228, 112.738),
(6, 'Masjid Agung Surabaya', 'religi', 'Rancang bangun arsitektur MAS dikerjakan oleh tim dari Institut Teknologi Sepuluh Nopember (ITS) Surabaya bersama konsultan ahli yang telah berpengalaman membangun masjid-masjid besar di Indonesia. Mengingat posisi lahan yang labil dengan tingkat kekerasan yang minim, maka pembuatan pondasi dilakukan dengan system pondasi dalam atau pakubumi. Tidak kurang dari 2000 tiang pancang bagi pondasi masjid ini.\r\n', 'Destinasi: Religi', 'Jl. Masjid Al-Akbar Utara No.1\r\n', 'https://tourism.surabaya.go.id/storage/tour/1648711246_2.jpg', -7.33661, 112.706),
(7, 'Museum Perjuangan 10 Nop', 'sejarah', 'Museum Sepuluh Nopember dibangun untuk memperkuat keberadaan Tugu Pahlawan bangunan museum terdiri dari dua lantai lantai satu digunakan untuk pemeran 10 patung yang melambangkan bentuk Semangat perjuangan rakyat Surabaya dan orasi Bung Tomo di lantai dua ruang auditorium merupakan Ruang pameran senjata dan foto-foto dokumenter dan lokasi peninggalan Bung Tomo. Museum Perjuangan 10 November didirikan untuk memperingati upaya heroik Surabaya selama pertempuran melawan pasukan sekutu\r\n', 'Destinasi: Museum\r\nOpen: Senin – Kamis : 08.00 – 15.00 | Jumat : 08.00 – 14.00\r\n| Sabtu & Minggu : 07.00 – 14.00 | Hari Besar Keagamaan LIBUR\r\nTiket: Rp. 5.000,- Pelajar: Gratis\r\nFasilitas : Ruang Diorama Elektronik - Ruang Auditorium - Ruang Perpustakaan - Ruang Diorama - Ruang Kidzone dan Laktasi - Musholla - Parkir Area - Public Space (Lapangan) – AC\r\n', 'Jl. Pahlawan', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_2_01.jpg', -7.24584, 112.729),
(8, 'Monumen Kapal Selam', 'sejarah', 'Monumen ini didirikan sebagai pengingat akan kebesaran Indonesia sebagai negara Bahari. Dengan negara kepulauan yang terdiri atas 17.000 pulau dan dua per tiga wilayah adalah samudra, identitas sebagai negara Bahari tentu adalah hal yang melekat di negara ini. Untuk itu, keberadaan Angkatan Laut merupakan hal yang esensial dan bagian dari jati diri bangsa.\r\n', 'Destinasi: Museum\r\nOpen: Setiap Hari : 08.00 – 22.00\r\nTiket: Rp. 15000/orang\r\nFasilitas : Video Rama, Musik Life, Kolam Renang untuk anak-anak dan Rekreasi Air di sungai Kalimas. Sebuah Stan suvenir dan area parkir.', 'Jl. Pemuda No.39, Embong Kaliasin, Genteng', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_25_01.jpg', -7.26543, 112.742),
(9, 'Museum House of Sampoerna', 'sejarah', 'Museum rokok ini dulunya adalah pabrik rokok pertama Sampoerna. Saat ini bangunan bergaya kolonial Belanda yang dibangun sekitar tahun 1862 termasuk dalam situs bersejarah yang dilestarikan di Surabaya. Pada masa Belanda, bangunan ini adalah panti asuhan yang dikelola Belanda. Kemudian pada tahun 1932 dibeli oleh Liem Seeng Tee yang menjadi pendiri Sampoerna dan menjadi tempat pertama produksi rokok Sampoerna. Saat ini, bangunan ini termasuk dalam situs sejarah yang dilestarikan di Surabaya', 'Destinasi: Museum\r\nOpen: Setiap Hari / 09.00-19.00\r\nTiket: Gratis\r\nFASILITAS :  pemrosesan tembakau dan cengkeh, peracikan, melinting rokok dan pengemasan, pencetakan dan pemroses bahan jadi', 'Jl. Taman Sampoerna No.6, Pabean Cantian', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_16_01.jpg', -7.23082, 112.726),
(10, 'Taman Bungkul', 'keluarga', 'Awal mula nama taman bungkul memang tidak lepas dari nama seorang tokoh yang sangat berpengaruh dalam penyebaran agama Islam di wilayah Surabaya dan sekitarnya, dia adalah Ki Ageng Supo yang kemudian mendapat gelar Sunan Bungkul atau Mbah Bungkul yang makamnya terdapat di belakang taman ini dan sekaligus menjadi tempat bagi para peziarah.', 'Destinasi: Taman\r\nOpen: 24 jam\r\nTiket: Gratis\r\nFASILITAS :  skateboard track dan BMX track, jogging track, plaza (panggung untuk live performance berbagai jenis entertainment), zona akses Wi-Fi gratis, telepon umum, area green park dengan kolam air mancur, taman bermain anak-anak hingga pujasera', 'Jl. Taman Bungkul, Darmo, Kec. Wonokromo, Kota Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_91_02.jpg', -7.29135, 112.731),
(11, 'Kebun Binatang Surabaya', 'keluarga', 'Kebun Binatang Surabaya (KBS)adalah salah satu kebun binatang yang populer di Indonesia dan terletak di Surabaya. KBS merupakan kebun binatang yang pernah terlengkap se-Asia Tenggara, di dalamnya terdapat lebih dari 351 spesies satwa yang berbeda yang terdiri lebih dari 2.806 binatang. Termasuk di dalamnya satwa langka Indonesia maupun dunia terdiri dari Mamalia, Aves, Reptilia, dan Pisces.', 'Destinasi: Keluarga\r\nBuka: Setiap Hari: 07:00 – 17:00\r\nTiket: Daftar online. Rp. 15.000,-/orang\r\nFasilitas :  Area parkir, Toilet, Spot kuliner, Wahana permainan, Spot selfie, Perpustakaan.\r\n', 'Jl. Setail No. 1, Darmo, Wonokromo', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_77_01.jpg', -7.29595, 112.728),
(12, 'Pasar Atom', 'pusatoleh', 'Pasar ini dikembangkan dengan konsep modern yang tertata dan sistem zona. Gedung ini berdiri di atas lahan seluas 6 hektare. Jumlah stand di Pasar Atom sekitar 2.000 stand. Sedangkan di Pasar Atom Mall sekitar 600 stand.\r\nPasar Atom ini terdapat toko perhiasan emas yang menjual emas berstandar internasional  dengan beragam model. Pasar Atom juga dapat ditemui butik pakaian perempuan dengan konsep modern, pakaian anak-anak, perlengkapan bayi, ibu hamil, dan ATM', 'Destinasi: Pusat oleh-oleh\r\nBuka: Setiap Hari: 10.00 – 17.00\r\nFasilitas :   eskalator, mushola, ATM Gallery, toilet, pusat informasi dan puluhan counter', 'Jl. Bunguran No.45, Bongkaran, Kec. Pabean Cantian, Kota Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_92_01.jpg', -7.24171, 112.735),
(13, 'Pasar Turi', 'pusatoleh', 'pasar Turi, adalah salah satu bangunan pasar yang fenomenal di Surabaya. Fenomenal karena walaupun seringkali di lumat si jago merah, tidak membuat pasar ini benar-benar tumbang dan hilang di makan waktu\r\nPasar yang berada di bawah Unit Pelaksana Pusat Pertokoan dan Perbelanjaan Pasar Turi ini selalu tidak pernah sepidikunjungi oleh mereka yang ingin berbelanja murah. Pasar Turi merupakan pasar untuk keperluan grosir, dimana semua barang lengkap tersedia', 'Destinasi: Pusat oleh-oleh\r\nBuka: Setiap Hari: 08.00 – 16.00\r\nFasilitas :   eskalator, mushola, ATM Gallery, toilet, pusat informasi dan puluhan counter\r\n', 'Jl. Pasar Turi, Kota Surabay', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_93_03.jpg', -7.246, 112.732),
(14, 'Depot Bu Rudy', 'pusatoleh', 'Kesuksesan demi kesuksesan telah diraihnya. Bermula dari warung mobil, pada akhirnya Bu Rudy berhasil membuka tujuh cabang di Surabaya. Depot pusat, rumah kedua bagi Bu Rudy, bertempat di Dharmahusada. Disusul oleh cabang Kupang Indah, Anjasmoro, Pasar Atum dan Pasar Atum Mall, Pusat Grosir Surabaya, Gresik, dan yang terbaru adalah Bu Rudy Online.', 'Destinasi: Pusat oleh-oleh\r\nBuka: Setiap Hari: 07.30 – 17.00\r\n', 'Jl. Dharmahusada No. 140, Surabaya', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_99_01.jpg', -7.26732, 112.77),
(15, 'Museum Pendidikan', 'sejarah', 'Museum Pendidikan Surabaya merupakan museum tematik yang didirikan sebagai langkah pelestarian sejarah dan budaya bangsa dengan tujuan untuk mendukung kegiatan edukasi, riset dan rekreasi di Kota Surabaya. Diresmikan oleh Walikota Surabaya Tri Rismaharini pada 25 November 2019. yang menyimpan bukti materiil Pendidikan pada masa Pra-Aksara, Masa Klasik, Masa Kolonial dan Masa Kemerdekaan', 'Destination: Museum\r\nOpen: Selasa-Minggu / 08.00-16.00 .Senin Tutup\r\nTiket: Gratis\r\nFasilitas :  Diorama Kelas - Parkir Area - Public Space (Taman) - Toilet - Ruang Laktasi - Musholla – AC', 'Jl. Genteng Kali No. 10', 'https://bappedalitbang.surabaya.go.id/ecobis/upload/wisata/sejarah/detail_162_08.jpg', -7.25621, 112.743);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_wisata`
--
ALTER TABLE `menu_wisata`
  ADD PRIMARY KEY (`id_wisata`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_wisata`
--
ALTER TABLE `menu_wisata`
  MODIFY `id_wisata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
